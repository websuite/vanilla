import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { VanillaArticleComponent } from './elements/vanilla-article/vanilla-article.component';
import { VanillaSignupComponent } from './elements/vanilla-signup/vanilla-signup.component';
import { VanillaPageComponent } from './pages/vanilla-page/vanilla-page.component';


// temporary
const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: '/app',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'app',
  //   component: AppComponent
  // },
  {
    path: '',
    component: AppComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
