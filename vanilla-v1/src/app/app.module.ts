import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { MaterialComponentsModule } from './material-components.module';

import { VanillaViewDataService } from './services/vanilla-view-data.service';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { VanillaArticleComponent } from './elements/vanilla-article/vanilla-article.component';
import { VanillaSignupComponent } from './elements/vanilla-signup/vanilla-signup.component';
import { VanillaPageComponent } from './pages/vanilla-page/vanilla-page.component';

import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    VanillaPageComponent,
    VanillaArticleComponent,
    VanillaSignupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    MaterialComponentsModule,
    AppRoutingModule
  ],
  providers: [
    VanillaViewDataService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
