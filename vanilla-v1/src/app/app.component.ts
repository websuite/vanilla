import { Component, OnInit } from '@angular/core';

import { VanillaViewDataService } from './services/vanilla-view-data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;

  constructor(private viewDataService: VanillaViewDataService) {
  }

  ngOnInit(): void {
    this.title = this.viewDataService.appTitle;
  }

}
