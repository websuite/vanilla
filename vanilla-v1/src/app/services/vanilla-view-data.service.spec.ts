import { TestBed, inject } from '@angular/core/testing';

import { VanillaViewDataService } from './vanilla-view-data.service';

describe('ViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VanillaViewDataService]
    });
  });

  it('should be created', inject([VanillaViewDataService], (service: VanillaViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
