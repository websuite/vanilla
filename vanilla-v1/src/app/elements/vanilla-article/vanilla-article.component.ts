import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { VanillaViewDataService } from '../../services/vanilla-view-data.service';


@Component({
  selector: 'ws-vanilla-article',
  templateUrl: './vanilla-article.component.html',
  styleUrls: ['./vanilla-article.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VanillaArticleComponent implements OnInit {

  title: string;
  mainCopyHTML: string;

  constructor(private viewDataService: VanillaViewDataService) {
  }

  ngOnInit() {
    this.title = this.viewDataService.articleTitle;
    this.mainCopyHTML = this.viewDataService.articleContentHTML;
  }

}
