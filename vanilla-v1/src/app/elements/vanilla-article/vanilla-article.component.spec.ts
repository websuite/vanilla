import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VanillaArticleComponent } from './vanilla-article.component';

describe('VanillaArticleComponent', () => {
  let component: VanillaArticleComponent;
  let fixture: ComponentFixture<VanillaArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VanillaArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VanillaArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
