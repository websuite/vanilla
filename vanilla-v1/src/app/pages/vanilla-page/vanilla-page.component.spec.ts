import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VanillaPageComponent } from './vanilla-page.component';

describe('VanillaPageComponent', () => {
  let component: VanillaPageComponent;
  let fixture: ComponentFixture<VanillaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VanillaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VanillaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
